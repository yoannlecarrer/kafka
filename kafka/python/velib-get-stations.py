import json
import time
import urllib.request
from kafka import KafkaProducer

API_KEY = "XXX" # Récupérer sa clé api en créant son compte sur le site https://developer.jcdecaux.com/#/home
url = "https://api.jcdecaux.com/vls/v1/stations?apiKey={}".format(API_KEY)

producer = KafkaProducer(bootstrap_servers=['localhost:9092', 'localhost:9093'])

empty_stations = []

def check_empty_station(station):
    if station["available_bikes"] == 0 and station["number"] not in empty_stations:
        empty_stations.append(station["number"])
        producer.send("empty-stations", json.dumps(station).encode())
    elif station["available_bikes"] > 0 and station["number"] in empty_stations:
        empty_stations.remove(station["number"])
        producer.send("empty-stations", json.dumps(station).encode())
    return

while True:
    response = urllib.request.urlopen(url)
    stations = json.loads(response.read().decode())
    for station in stations:
        check_empty_station(station)
        producer.send("velib-stations", json.dumps(station).encode(),
                      key=str(station["number"]).encode())
    print("{} Produced {} station records".format(time.time(), len(stations)))
    time.sleep(1)
    