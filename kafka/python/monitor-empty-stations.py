import json
from kafka import KafkaConsumer

empty_stations = {}
empty_stations_count = {}

consumer = KafkaConsumer("empty-stations", bootstrap_servers=['localhost:9092', 'localhost:9093'],
                         group_id="empty-stations-monitor")

for message in consumer:
    station = json.loads(message.value.decode())
    station_number = station["number"]
    contract = station["contract_name"]
    address = station["address"]

    if contract not in empty_stations:
        empty_stations[contract] = set()
        empty_stations_count[contract] = 0

    if station_number not in empty_stations[contract] and station["available_bikes"] == 0:
        empty_stations[contract].add(station_number)
        empty_stations_count[contract] += 1
        print(f"La station {station_number} est devenu vide à {address}, {contract}. "
              f"Maintenant {empty_stations_count[contract]} stations sont vide à {contract}.")
    
    elif station_number in empty_stations[contract] and station["available_bikes"] > 0:
        empty_stations[contract].remove(station_number)
        empty_stations_count[contract] -= 1
        print(f"La station {station_number} n'est plus vide à l'adresse {address}, {contract}. "
              f"Maintenant {empty_stations_count[contract]} stations sont vide à {contract}.")